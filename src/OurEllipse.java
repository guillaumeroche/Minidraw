import java.awt.geom.*;
import java.awt.*;

/**
 * D�finit les caract�ristiques d'une ellipse
 * 
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class OurEllipse extends OurShape
{

    /**
     * Cr�� une ellipse
     * @param x     position sur x (en pixels) de l'ellipse
     * @param y     position sur y (en pixels) de l'ellipse
     * @param height    hauteur de l'ellipse en pixels
     * @param width     largeur de l'ellipse en pixels
     * @param colour    couleur de l'ellipse
     * @param filled    d�finit si l'ellipse est remplie ou non
     */
    public OurEllipse(int x, int y, int height, int width, Color color, boolean filled, int transluency)
    {
        // Appel au constructeur de la super classe
        super(x, y, height, width, color, filled, transluency);
    }
    
    /**
     * Renvoie le perimetre de l'ellipse
     * @return perimetre
     */
    public double perimeter()
    {
        return 2 * Math.PI * Math.sqrt((height * height + width * width) / 2);
    }
    
    /**
     * Renvoie l'aire de l'ellipse en pixels
     * @return aire
     */
    public double area()
    {
        return Math.PI*height*width;
    }
    
    /**
     * Dessine l'ellipse
     */
    public void draw(Graphics g)
    {
        g.setColor(colour);
        if(filled == true)
        {
            g.fillOval(x, y, width, height);
        }
        else
        {
            g.drawOval(x, y, width, height);
        }
    }
}
