import java.awt.*;
import javax.swing.*;

/**
 * Panneau d'information affichant divers param�tres
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class StatusPanel extends JPanel
{
    //�tiquettes d'information
    private JLabel x;
    private JLabel y;
    private JLabel height;
    private JLabel width;
    private JLabel area;
    private JLabel perimeter;
    
    /**
     * Cr�ation du Panneau d'�tat
     */
    public StatusPanel()
    {
        this.add(new JLabel("Current Values:", 0));
        
        x = new JLabel("x:");
        this.add(x);
        
        y = new JLabel("y:");
        this.add(y);
        
        height = new JLabel("Height:");
        this.add(height);
        
        width = new JLabel("Width:");
        this.add(width);
        area = new JLabel("Area:");
        this.add(area);
        
        perimeter = new JLabel("Perimeter:");
        this.add(perimeter);
        
        this.add(new JSeparator());
    }
    
    /**
     * Mise � jour des valeurs � afficher
     * @param x position de la forme courante suivant x
     * @param y position de la forme courante suivant y
     * @param height hauteur de la forme courante
     * @param width largeur de la forme courante
     * @param perimeter perimetre de la forme courante
     * @param area aire de la forme courante
     */
    public void setValues(int x, int y, int height, int width, double perimeter, double area)
    {
        //On modifie les valeurs seulement en cas de validit� de celles-ci
        if(x != -1 && area != 0)
        {
            this.x.setText("x: " + x);
            this.y.setText("y: " + y);
            this.height.setText("Height: " + height);
            this.width.setText("Width: " + width);
            this.area.setText("Area: " + (int)area);
            this.perimeter.setText("Perimeter: " + (int)perimeter);
        }
    }
}
