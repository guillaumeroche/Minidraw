import java.awt.*;
import javax.swing.*;
import java.lang.Math;
import java.io.*;

/**
 * Contient toutes les m�thodes et champs communs aux diff�rentes formes
 * 
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public abstract class OurShape implements Serializable
{
    // variables d'instances communes aux diff�rentes formes
    //coordonn�es
    protected int x;
    protected int y;
    //taille
    protected int height;
    protected int width;
    //couleur
    protected Color colour;
    //remplissage
    protected boolean filled;
    //transparance
    protected int transluency;

    /**
     * Cr�� une nouvelle forme
     * @param x     position sur x (en pixels) de la forme
     * @param y     position sur y (en pixels) de la forme
     * @param height    hauteur de la forme en pixels
     * @param width     largeur de la forme en pixels
     * @param colour    couleur de la forme
     * @param filled    d�finit si la forme est remplie ou non
     */
    public OurShape(int x, int y, int height, int width, Color colour, boolean filled, int transluency)
    {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.colour = colour;
        this.filled = filled;
        this.transluency = transluency;
    }

    /**
     * Calcule le perimetre de la forme
     * @return le perimetre de la forme
     */
    public abstract double perimeter();
    
    /**
     * Calcule l'aire de la forme
     * @return l'aire de la forme
     */
    public abstract double area();
    
    /**
     * Dessine la forme
     * @param g Graphics dans lequel est dessin� la forme
     */
    public abstract void draw(Graphics g);
    
    /**
     * Effectue une translation de la forme
     * @param deltaX    translation suivant x en pixels
     * @param deltaY    translation suivant y en pixels
     */
    public void translate(int deltaX, int deltaY)
    {
        x += deltaX;
        y += deltaY;
    }
    
    /**
     * Effectue une homot�thie sur la forme
     * @param factor    facteur de grossissement
     */
    public void scale(float factor)
    {
        if(height > 20 && width > 20 || factor > 1 )
        {
            int oldHeight = height;
            int oldWidth = width;
            height = (int)(height * factor);
            width = (int)(width * factor);
            y = y + (oldHeight - height) / 2;
            x = x + (oldWidth - width) / 2;
        }
    }
    
    /**
     * D�finit la couleur d'une forme
     * @param c     nouvelle couleur
     */
    
    public void setColor(Color c)
    {
        this.colour = c;
    }
    
    /**
     * Renvoie la couleur courante de la forme
     * @return  couleur courante
     */
    public Color getColor()
    {
        return this.colour;
    }
    
    /**
     * Remplis la forme
     */
    public void setFilled()
    {
        this.filled = true;
    }
    
    /**
     * Vide la forme
     */
    public void remFilled()
    {
        this.filled = false;
    }
    
    /**
     * Teste si la forme est remplie
     * @return  true si la forme est remplie, false sinon
     */
    public boolean getFilled()
    {
        return filled;
    }
    
    /**
     * V�rifie si un point p est � l'interieur de la bounding box
     * @param p point � tester
     * @return true si le point est � l'int�rieur, false sinon
     */
    public boolean isInside(Point p)
    {
        return p.getX() >= x && p.getX() <= x + width && p.getY() >= y && p.getY() <= y + height;
    }
    
    /**
     * Retourne la coordonn�e suivant x de la forme
     * @return x
     */
    public int getX()
    {
        return x;
    }
    
    /**
     * Retourne la coordonn�e suivant y de la forme
     * @return y
     */
    public int getY()
    {
        return y;
    }
    
    /**
     * Retourne la hauteur de la forme
     * @return height
     */
    public int getHeight()
    {
        return height;
    }
    
    /**
     * Retourne la largeur de la forme
     * @return width
     */
    public int getWidth()
    {
        return width;
    }
    
    /**
     * Fixe le niveau de transparence
     * @param transluency entre 0 (transparent) et 100 (opaque)
     */
    public void setTransluency(int transluency)
    {
        this.transluency = transluency;
    }
    
    /**
     * Retourne le niveau de transparence
     * @return entre 0 (transparent) et 100 (opaque)
     */
    public int getTransluency()
    {
        return transluency;
    }

}
