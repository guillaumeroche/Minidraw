import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;

/**
 * Panneau d'outils
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class ToolPanel extends JPanel
{
    private DrawPanel drawing;
    private JCheckBox selectMode;
    private JCheckBox fill;
    private boolean select;
    private JButton changeColor;
    private JButton changeBackColor;
    private JColorChooser colorChooser;
    private JSlider tranSlider;
    
    /**
     * Cr�� un nouveau ToolPanel permettant de s�lectionner les options de dessin
     * @param drawinG panneau de dessin auquel indiquer les options de dessin
     */
    public ToolPanel(DrawPanel drawinG)
    {
        this.drawing = drawinG;
        colorChooser = new JColorChooser();
        colorChooser.setPreviewPanel(new JPanel());
        select = false;
        
        this.add(new JLabel("Options:", 0));
        
        //choix du remplissage
        fill = new JCheckBox("Fill");
        this.add(fill);
        fill.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                drawing.setFilled();
            }
        });
        
        //checkbox permetant de passer du mode dessin au mode s�lection (et vice versa)
        selectMode = new JCheckBox("Select Mode");
        this.add(selectMode);
        selectMode.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                drawing.setSelect();
                select = !select;
            }
        });
        
        // bouton de modification de couleur de forme
        changeColor = new JButton("Change Shape Color");
        this.add(changeColor);
        changeColor.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                
                Color newColor = JColorChooser.showDialog(colorChooser, "Choose a Color", drawing.getColor());
                if(newColor != null)
                {
                    drawing.setColor(newColor);
                }
            }
        });
        
        // bouton de modification de couleur de fond
        changeBackColor = new JButton("Change Background Color");
        this.add(changeBackColor);
        changeBackColor.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                
                Color newColor = JColorChooser.showDialog(colorChooser, "Choose a Color", drawing.getColor());
                if(newColor != null)
                {
                    drawing.setBackground(newColor);
                }
            }
        });
        
        this.add(new JLabel("Opacity:"));
        
        //r�glage de l'opacit� / transparence
        tranSlider = new JSlider();
        this.add(tranSlider);
        tranSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                drawing.setTrans(tranSlider.getValue());
            }
        });
            
        
        this.add(new JSeparator());
    }
    
    /**
     * Coche ou d�coche la checkbox "Fill"
     * @param tmpfill true pour cocher, false pour d�cocher
     */
    public void checkFill(boolean tmpfill)
    {
        fill.setSelected(tmpfill);
    }
    
}
