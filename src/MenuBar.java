import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Barre de menu de la fen�tre
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class MenuBar extends JMenuBar
{
    DrawPanel drawing;
    JMenu mainMenu;
    JMenuItem item;

    /**
     * Cr�� une barre de menu
     * @param drawinG panneau de dessin avec lequel dialoguer
     */
    public MenuBar(DrawPanel drawinG)
    {
        this.drawing = drawinG;
        
        mainMenu = new JMenu("File");
        this.add(mainMenu);

        item = new JMenuItem("Save");
        item.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                drawing.save();
            }
        });
        mainMenu.add(item);
                
        item = new JMenuItem("Load");
        item.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                drawing.load();
            }
        });
        mainMenu.add(item);
        
        item = new JMenuItem("Export to JPG");
        item.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                drawing.export();
            }
        });
        mainMenu.add(item);
        
        mainMenu.addSeparator();
        item = new JMenuItem("Exit");
        item.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                System.exit(0);
            }
        });
        mainMenu.add(item);
        

        mainMenu = new JMenu("Edit");
        this.add(mainMenu);
        
        item = new JMenuItem("Cancel");
        item.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                drawing.cancel();
            }
        });
        mainMenu.add(item);
        item = new JMenuItem("Restore");
        item.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                drawing.restore();
            }
        });
        mainMenu.add(item);
        item = new JMenuItem("Clear");
        item.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                drawing.clear();
                drawing.setColor(Color.blue);
            }
        });
        mainMenu.add(item);
    }
}
