import java.awt.*;
import java.awt.Graphics2D;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

/**
 * Panneau de Dessin. Contient toutes les formes dessin�es, et g�re les �venements souris.
 * @author Lencou Romain, Roche Guillaume
 * @version 1
 */
public class DrawPanel extends JPanel
{
   //coordonn�es + angle du parallelogramme
   private int x1, y1, x2, y2, angle;
   private int transluency;
   //nom de forme courante
   private String shape;
   //forme courante
   private OurShape myShape;
   //forme s�lectionn�e
   private OurShape selectedShape;
   private OurRectangle contShape;
   //couleur
   private Color color;
   private boolean filled, select, selected;
   //stockage des formes affich�es
   private Vector shapeVector;
   //stockage des formes supprim�e
   private Vector cancelVector;
   private JPopupMenu popupMenu;
   private StatusPanel statusPanel;

   /**
    * Constructeur, initialise les diff�rents champs, ainsi que les �couteurs d'�v�nements
    */
   public DrawPanel(StatusPanel status)
   {
      this.statusPanel = status;
      
      shapeVector = new Vector();
      cancelVector = new Vector();
      filled = false;
      select = false;
      selected = false;
      shape = "square";
      color = Color.red;
      angle = 10;
      transluency = 50;
      
      //Ajout d'un �couteur sur la souris
      addMouseListener(new MouseAdapter()
      {
            //bouton de souris appuy�
            public void mousePressed(MouseEvent event)
            {
               x1 = event.getX();
               y1 = event.getY();
               
               //s'il s'agit du bouton droit, on cr�� un menu muni
               if(event.getButton() == 3)
               {
                    x2 = event.getX();
                    y2 = event.getY();
                    popupMenu = new JPopupMenu();
               }
               
               //Si on est en mode s�lection, on s�lectionne une forme pr�sente � l'endroit du click.
               //Si plusieurs formes sont pr�sentes � cet endroit, on s�lectionne la derni�re cr��e
               if(select)
               {
                   Iterator it = shapeVector.iterator();
                   selected = false;
                   while(it.hasNext())
                   {
                       myShape = (OurShape) it.next();
                       if(myShape.isInside(new Point(x1, y1)))
                       {
                           selectedShape = myShape;
                           selected = true;
                       }
                   }
                   //Si on a cliqu� avec le bouton droit, on rajoute l'�l�ment remove au menu, permettant de supprimer la forme s�lectionn�e
                   if(event.getButton() == 3 && selected)
                   {
                        JMenuItem item = new JMenuItem("Remove");
                        item.addActionListener(new ActionListener()
                        {
                            public void actionPerformed(ActionEvent e)
                            {
                                remove();
                            }
                        });
                        popupMenu.add(item);
                    }
                    repaint();
                }
                //Si on a cliqu� avec le bouton droit, on ajoute au menu un bouton "clear", et on l'affiche
                if(event.getButton() == 3)
                {
                    JMenuItem item = new JMenuItem("Clear");
                    item.addActionListener(new ActionListener()
                    {
                        public void actionPerformed(ActionEvent e)
                        {
                            clear();
                        }
                    });
                    popupMenu.add(item);
                    popupMenu.show(event.getComponent() ,x1 ,y1);
                }

            }

            //bouton de souris rel�ch�
            public void mouseReleased(MouseEvent event)
            {
                //si on n'est pas en mode s�lection, et que le bouton n'est pas le droit, on stocke la forme courante dans le vecteur
                if(!select && event.getButton() != 3)
                {
                    x2 = event.getX();
                    y2 = event.getY();
                    shapeVector.add((OurShape)myShape);
                    reset();
                    repaint();
                }
            }
            
      });

      //Ajout d'un �couteur sur la souris
      addMouseMotionListener(new MouseMotionAdapter()
      {
            //la souris bouge avec un bouton maintenu enfon��
            public void mouseDragged(MouseEvent event)
            {
                //Si on n'est pas en mode s�lection, on modifie les champs x2 et y2 de mani�re � ce que la forme "suive" le curseur en mode dessin
                if(!select && event.getButton() != 3)
                {
                   x2 = event.getX();
                   y2 = event.getY();
                   repaint();
                }
                //Si une forme est s�lectionn�e, on la d�place
                else if(selected)
                {
                    x2 = event.getX();
                    y2 = event.getY();
                    selectedShape.translate(x2 - x1, y2 - y1);
                    repaint();
                    x1 = x2;
                    y1 = y2;
                }
            }
       });
       
      //Ajout d'un �couteur sur la souris       
       addMouseWheelListener(new MouseWheelListener()
       {
           //On utilise la molette
           public void mouseWheelMoved(MouseWheelEvent e)
           {
               //en mode s�lection, on modifie la taille de la forme
               if(select && selected)
               {
                   if(e.getWheelRotation() > 0)
                   {
                       selectedShape.scale(0.8F);
                   }
                   else
                   {
                       selectedShape.scale(1.2F);
                   }
                }
                repaint();
            }
        });
   }
   
   /**
    * Affiche les diff�rentes formes cr��es
    * @param graphics permet de dessiner les formes
    */
    public void paintComponent(Graphics graphics)
    {
       //on passe en graphics2d (antialias, transparence, ...)
       Graphics2D g = (Graphics2D) graphics;
       //on active l'antialiasing
       g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
       super.paintComponent(g);
       
       //on parcours le vecteur pour tout afficher
       Iterator it = shapeVector.iterator();
       while(it.hasNext())
       {
           myShape = (OurShape) it.next();
           if(myShape == selectedShape && selected && select)
           {
               g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1F));
               contShape = new OurRectangle(myShape.getX() - 1, myShape.getY() - 1, myShape.getHeight() + 2, myShape.getWidth() + 2, Color.black, false, 100);
               statusPanel.setValues(myShape.getX(), myShape.getY(), myShape.getHeight(), myShape.getWidth(), myShape.perimeter(), myShape.area());
               contShape.draw(g);
           }
           g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float)myShape.getTransluency() / 100));
           myShape.draw(g);
       }
       //en mode dessin, on affiche la forme en train d'�tre dessin�e (pas encore stock�e)
       if(!select)
       {
           if(shape == "ellipse")
           {
               myShape = new OurEllipse(Math.min(x1, x2), Math.min(y1, y2), Math.abs(y1 - y2), Math.abs(x1 - x2), color, filled, transluency);
           }
          
           if(shape == "square")
           {
               myShape = new OurSquare(Math.min(x1, x2), Math.min(y1, y2), Math.min(Math.abs(y1 - y2), Math.abs(x1 - x2)), color, filled, transluency);
           }
           
           if(shape == "rectangle")
           {
               myShape = new OurRectangle(Math.min(x1, x2), Math.min(y1, y2), Math.abs(y1 - y2), Math.abs(x1 - x2), color, filled, transluency);
           }
          
           if(shape == "parallelogram")
           {
               myShape = new OurParallelogram(Math.min(x1, x2), Math.min(y1, y2), Math.abs(y1 - y2), Math.abs(x1 - x2), color, filled, angle, transluency);
           }
          
           if(shape == "circle")
           {
               myShape = new OurCircle(Math.min(x1, x2), Math.min(y1, y2), Math.abs(y1 - y2), color, filled, transluency);
           }
           statusPanel.setValues(myShape.getX(), myShape.getY(), myShape.getHeight(), myShape.getWidth(), myShape.perimeter(), myShape.area());
           myShape.draw(g);
       }
      
    }
   
    /**
     * Permet de choisir la forme � dessiner
     * @param shape forme � dessiner: square, rectangle, circle, parallelogram, ellipse
     */
    public void setShape(String shape)
    {
       this.shape = shape;
    }
    
    /**
     * Permet d'activer ou non le remplissage des formes
     */
    public void setFilled()
    {
        filled = !filled;
        if(select && selected)
        {
            if(filled)
            {
                selectedShape.setFilled();
            }
            else
            {
                selectedShape.remFilled();
            }
            repaint();
        }
    }
    
    /**
     * Indique si on est en mode remplissage ou pas
     * @return true si on est en mode remplissage, false sinon
     */
    public boolean isFilled()
    {
        if(selected)
        {
            return selectedShape.getFilled();
        }
        return filled;
    }
    
    /**
     * Change de mode d'utilisation en mode "dessin" ou "s�lection"
     */
    public void setSelect()
    {
        select = !select;
        reset();
        repaint();
    }
    
    /**
     * Indique si une forme est s�lectionn�e
     * @return true si une forme est s�lectionn�e, false sinon
     */
    public boolean getSelected()
    {
        return selected;
    }
    
    /**
     * Indique le mode d'utilisation courant
     * @return true pour le mode "s�lection", false pour le mode "dessin"
     */
    public boolean getSelect()
    {
        return select;
    }
    
    /**
     * Modifie la couleur courante
     * @param color nouvelle couleur
     */
    public void setColor(Color color)
    {
        this.color = color;
        if(select && selected)
        {
            selectedShape.setColor(color);
            repaint();
        }
    }
    
    /**
     * Modifie le niveau de transparence d'une forme
     * @param transluency entre 0 (transparent) et 100 (opaque)
     */
    public void setTrans(int transluency)
    {
        this.transluency = transluency;
        if(select && selected)
        {
            selectedShape.setTransluency(transluency);
            repaint();
        }
    }
    
    /**
     * Indique la couleur courante
     * @return couleur courante
     */
    public Color getColor()
    {
        return this.color;
    }
    
    /**
     * Bidouille pour �viter des bugs graphiques
     */
    private void reset()
    {
        x1 = -1;
        x2 = -1;
        y1 = -1;
        y2 = -1;
    }
    
    /**
     * Supprime toutes les formes du dessin
     */
    public void clear()
    {
        shapeVector.removeAllElements();
        reset();
        statusPanel.setValues(0,0,0,0,0,0);
        this.setBackground(Color.WHITE);
        repaint();
    }
    
    /**
     * Efface la derni�re forme cr��e
     */
    public void cancel()
    {
        if(!shapeVector.isEmpty())
        {
            cancelVector.add(shapeVector.lastElement());
            shapeVector.remove(shapeVector.lastElement());
            reset();
            repaint();
        }
    }
    
    /**
     * R�ins�re la derni�re forme supprim�e
     */
    public void restore()
    {
        if(!cancelVector.isEmpty())
        {
            shapeVector.add(cancelVector.lastElement());
            cancelVector.remove(cancelVector.lastElement());
            reset();
            repaint();
        }
    }
    
    /**
     * Supprime une forme
     */
    public void remove()
    {
        shapeVector.remove(selectedShape);
        cancelVector.add(selectedShape);
        selected = false;
        repaint();
    }
    
    /**
     * D�finit l'angle des parall�logrammes
     * @param angle nouvel angle
     */
    public void setAngle(int angle)
    {
        this.angle = angle;
        reset();
        repaint();
    }
    
    /**
     * Sauvegarde le dessin courant
     */
    public void save()
    {
        JFileChooser chooser = new JFileChooser("/mnt/hda8/coding/java/paint/");
        int returnVal = chooser.showSaveDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                FileOutputStream fos = new FileOutputStream(chooser.getSelectedFile().getName());
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(shapeVector);
            }
            catch(IOException e)
            {
                System.out.println("Error during file saving" + e);
            }
        }
    }
    
    /**
     * Charge un dessin de type MiniDraw
     */
    public void load()
    {
        JFileChooser chooser = new JFileChooser("/mnt/hda8/coding/java/paint/");
        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                FileInputStream fis = new FileInputStream(chooser.getSelectedFile().getName());
                ObjectInputStream ois = new ObjectInputStream(fis);
                shapeVector = (Vector)ois.readObject();
                repaint();
            }
            catch(IOException e)
            {
                System.out.println("Error during file loading" + e);
            }
            catch(ClassNotFoundException e)
            {
                System.out.println("Not Found" + e);
            }
        }
    }
    
    /**
     * Exporte le dessin courant au format JPG
     */
    public void export()
    {
        JFileChooser chooser = new JFileChooser("/mnt/hda8/coding/java/paint/");
        int returnVal = chooser.showSaveDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            BufferedImage myImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = myImage.createGraphics();
            this.paint(g2);
            g2.dispose();
            try
            {
                ImageIO.write(myImage,"jpeg",chooser.getSelectedFile());
            }
            catch(IOException exc)
            {
                exc.printStackTrace(); 
            }
            
        }
    }

}