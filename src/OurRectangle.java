import java.awt.Color;

/**
 * Définit les caractéristiques d'un rectangle
 * 
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class OurRectangle extends OurParallelogram
{
    /**
     * Constructeur de OurRectangle. Initialise les différents champs
     * @param x     position sur x (en pixels) du rectangle
     * @param y     position sur y (en pixels) du rectangle
     * @param height    hauteur du rectangle en pixels
     * @param width     largeur du rectangle en pixels
     * @param colour    couleur du rectangle
     * @param filled    définit si le rectangle est rempli ou non
     */
	public OurRectangle(int x, int y, int height, int width, Color color, boolean filled, int transluency)
	{
		// Appel au constructeur de la super classe
		super(x, y, height, width, color, filled, 0, transluency);
	}

}
