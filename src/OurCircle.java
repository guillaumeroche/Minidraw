import java.awt.Color;

/**
 * D�finit les caract�ristiques d'un cercle
 * 
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class OurCircle extends OurEllipse
{


    /**
     * Cr�� un cercle
     * @param x     position sur x (en pixels) du carr�
     * @param y     position sur y (en pixels) du carr�
     * @param height    hauteur du carr� en pixels
     * @param width     largeur du carr� en pixels
     * @param colour    couleur du carr�
     * @param filled    d�finit si le carr� est rempli ou non
     */
	public OurCircle(int x, int y, int diameter, Color color, boolean filled, int transluency)
	{
		// initialise instance variables
		super(x, y, diameter, diameter, color, filled, transluency);
	}

}
