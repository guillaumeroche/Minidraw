import java.awt.*;

/**
 * Définit les caractéristiques d'un parallelogramme
 * 
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class OurParallelogram extends OurShape
{
    // variable angle: inclinaison du paralellograme
    protected double angle;

    /**
     * Constructeurr d'objets de la classe OurParallelogram
     * @param x     position sur x (en pixels) du parallelogramme
     * @param y     position sur y (en pixels) du parallelogramme
     * @param height    hauteur du parallelogramme en pixels
     * @param width     largeur du parallelogramme en pixels
     * @param colour    couleur du parallelogramme
     * @param filled    définit si le parallelogramme est rempli ou non
     * @param angle     angle du parallelogramme
     */
    public OurParallelogram(int x, int y, int height, int width, Color colour, boolean filled, double angle, int transluency)
    {
        // initialise instance variables
        super(x, y, height, width, colour, filled, transluency);
        this.angle = angle;
    }

    /**
     * renvoie le perimetre du parallelogramme
     * @return     perimetre
     */
    public double perimeter()
    {
        return (2 * (width - height * Math.tan(angle)) + 2 * height / Math.cos(angle));
    }
    
    /**
     * renvoie l'aire du parallelogramme
     * @return     aire
     */
    public double area()
    {
        return (height * (width - height * Math.tan(angle)));
    }
    
    /**
     * Dessine le parallelogramme
     * @param g     panneau dans lequel dessiner le parallelogramme
     */
    public void draw(Graphics g)
    {
        double angleR = Math.toRadians(angle);
        int[] xPoints = {x, (int)(x + height * Math.tan(angleR)), x + width, (int)(x + width - height * Math.tan(angleR))};
        int[] yPoints = {y + height, y, y, y + height};
        g.setColor(colour);
        if(filled == true)
        {
            g.fillPolygon(xPoints, yPoints, 4);
        }
        else
        {
            g.drawPolygon(xPoints, yPoints, 4);
        }
    }

}
