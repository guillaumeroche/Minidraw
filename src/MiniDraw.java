import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Classe principale utilisant les autres classes.
 * 
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class MiniDraw extends JFrame
{
    private DrawPanel drawing;
    private ShapePanel shapePanel;
    private ToolPanel toolPanel;
    private StatusPanel statusPanel;
    private MenuBar menuBar;
    
    /**
     * Cr�� un minidraw, affiche tous les composants et panneaux dans une seule fen�tre.
     */
    public MiniDraw()
    { 
        BorderLayout mainLayout = new BorderLayout();

        setTitle("MiniDraw");
        Container contentPane = this.getContentPane();

        contentPane.setLayout(mainLayout);
        
        statusPanel = new StatusPanel();
        statusPanel.setLayout(new GridLayout(8, 1));
        
        drawing = new DrawPanel(statusPanel);
        drawing.addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e)
            {
                if(drawing.getSelect())
                {
                    toolPanel.checkFill(drawing.isFilled());
                }
            }
        });
        drawing.setBackground(Color.WHITE);
        contentPane.add(drawing, mainLayout.CENTER);
        
        shapePanel = new ShapePanel(drawing);
        shapePanel.setLayout(new GridLayout(0,1));
        
        toolPanel = new ToolPanel(drawing);
        toolPanel.setLayout(new GridLayout(8, 1));
        
        JPanel sidePanel = new JPanel();
        sidePanel.setLayout(new GridLayout(4, 1));
        
        sidePanel.add(shapePanel);
        sidePanel.add(toolPanel);
        sidePanel.add(statusPanel);
        
        contentPane.add(sidePanel, mainLayout.WEST);
        
        menuBar = new MenuBar(drawing);
        this.setJMenuBar(menuBar);
        
        this.pack();
        this.setSize(800, 600);
        this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        this.setVisible(true);
    }
        
}

