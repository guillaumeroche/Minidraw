import java.awt.Color;

/**
 * D�finit les caract�ristiques d'un carr�
 * 
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class OurSquare extends OurRectangle
{

    /**
     * Cr�� un carr�
     * @param x     position sur x (en pixels) du carr�
     * @param y     position sur y (en pixels) du carr�
     * @param height    hauteur du carr� en pixels
     * @param width     largeur du carr� en pixels
     * @param colour    couleur du carr�
     * @param filled    d�finit si le carr� est rempli ou non
     */
    public OurSquare(int x, int y, int size, Color color, boolean filled, int transluency)
    {
		// Appel au constructeur de la super classe
        super(x, y, size, size, color, filled, transluency);
    }


}
