import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Applet de minidraw, permet d'utiliser MiniDraw dans une page web.
 * 
 * @author Lencou Romain, Roche Guillaume
 * @version 1
 */
public class MiniDrawApplet extends JApplet implements Runnable
{
    private DrawPanel drawing;
    private ShapePanel shapePanel;
    private ToolPanel toolPanel;
    private StatusPanel statusPanel;
    private MenuBar menuBar;
    private boolean mustStop;
    
    /**
     * Cr�� un minidraw, affiche tous les composants et panneaux dans une applet.
     */
    public void start()
    { 
        this.mustStop = false;
        (new Thread(this)).start();
        BorderLayout mainLayout = new BorderLayout();

//         setTitle("MiniDraw");
        Container contentPane = this.getContentPane();

        contentPane.setLayout(mainLayout);
        
        statusPanel = new StatusPanel();
        statusPanel.setLayout(new GridLayout(8, 1));
        
        drawing = new DrawPanel(statusPanel);
        drawing.addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e)
            {
                if(drawing.getSelect())
                {
                    toolPanel.checkFill(drawing.isFilled());
                }
            }
        });
        drawing.setBackground(Color.WHITE);
        contentPane.add(drawing, mainLayout.CENTER);
        
        shapePanel = new ShapePanel(drawing);
        shapePanel.setLayout(new GridLayout(0,1));
        
        toolPanel = new ToolPanel(drawing);
        toolPanel.setLayout(new GridLayout(0, 1));
        
        JPanel sidePanel = new JPanel();
        sidePanel.setLayout(new GridLayout(4, 1));
        
        sidePanel.add(shapePanel);
        sidePanel.add(toolPanel);
        sidePanel.add(statusPanel);
        
        contentPane.add(sidePanel, mainLayout.WEST);
        
        menuBar = new MenuBar(drawing);
        this.setJMenuBar(menuBar);
        this.setVisible(true);
    }
    
    /**
     * Stoppe l'ex�cution de l'applet
     */
    public void stop()
    {
        this.mustStop = true;
    }
    
    /**
     * Ex�cution de l'applet
     */
    public void run()
    {
        while(true)
        {
            this.paint(this.getGraphics());
            try
            {
                Thread.sleep(50000);
            }
            catch(Exception e)
            {
                repaint();
            }
            if(this.mustStop == true)
            {
                return;
            }
        }
    }
        
}