import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;

/**
 * Panneau de s�lection des formes
 * @author Lencou Romain - Roche Guillaume
 * @version 1
 */
public class ShapePanel extends JPanel
{
    private DrawPanel drawing;
    private ButtonGroup shapeSelect;
    private JRadioButton square;
    private JRadioButton rectangle;
    private JRadioButton circle;
    private JRadioButton parallelogram;
    private JRadioButton ellipse;
    private JRadioButton car;
    private JSlider angle;
    
    /**
     * Construit un nouveau panneau de formes permettant de s�lectionner la forme � dessiner
     * @param drawinG panneau de dessin auquel indiquer la forme � dessiner
     */
    public ShapePanel(DrawPanel drawinG)
    {
        this.drawing = drawinG;
        
        drawing.setShape("square");
        
        this.add(new JLabel("Shape selection:", 0));
        
        ButtonGroup shapeSelect = new ButtonGroup();
        
        JRadioButton circle = new JRadioButton("Circle");
        shapeSelect.add(circle);
        this.add(circle);
        circle.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                drawing.setShape("circle");
            }
        });
        
        JRadioButton ellipse = new JRadioButton("Ellipse");
        shapeSelect.add(ellipse);
        this.add(ellipse);
        ellipse.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                drawing.setShape("ellipse");
            }
        });
        
        JRadioButton square = new JRadioButton("Square");
        square.setSelected(true);
        shapeSelect.add(square);
        this.add(square);
        square.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                drawing.setShape("square");
            }
        });
        
        JRadioButton rectangle = new JRadioButton("Rectangle");
        shapeSelect.add(rectangle);
        this.add(rectangle);
        rectangle.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                drawing.setShape("rectangle");
            }
        });
        
        JRadioButton parallelogram = new JRadioButton("Parallelogram");
        shapeSelect.add(parallelogram);
        this.add(parallelogram);
        parallelogram.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                drawing.setShape("parallelogram");
            }
        });
        
        angle = new JSlider(0, 45, 10);
        this.add(angle);
        angle.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                drawing.setAngle(angle.getValue());
            }
        });
        
        this.add(new JSeparator());
    }

}
