import java.awt.*;

/**
 * class Car. Permet d'afficher un caract�re
 * 
 * @author Lencou Romain - Roche Guillaume 
 * @version 1
 */
public class Car
{
    // tableau de char
    private int x, y, size;
    private Color colour;
    private char car;

    /**
     * Constructor d'objets de la classe Car
     * @param x     position sur x (en pixels) du caract�re
     * @param y     position sur y (en pixels) du caract�re
     * @param size    taille du caract�re
     * @param colour    couleur du caract�re
     * @param car   caract�re � afficher
     */
    public Car(int x, int y, int size, Color colour, char car)
    {
        // Appel au constructeur de la super classe
        this.x = x;
        this.y = y;
        this.size = size;
        this.colour = colour;
        this.car = car;
    }
    
    /**
     * Affichage du caract�re
     * @param g     panneau dans lequel afficher le caract�re
     */
    public void draw(Graphics g)
    {
        char[] c = {this.car};
        g.setColor(colour);
        g.setFont(new Font("TimesRoman", Font.BOLD, this.size));
        g.drawChars(c, 0, 1, x, y);
    }
    
    /**
     * Effectue une translation du caract�re
     * @param deltaX    translation suivant x en pixels
     * @param deltaY    translation suivant y en pixels
     */
    public void translate(int deltaX, int deltaY)
    {
        x += deltaX;
        y += deltaY;
    }

    /**
     * Effectue une homot�thie sur le caract�re
     * @param factor    facteur de grossissement
     */
    public void scale(double factor)
    {
        size = (int)(size * factor);
    }
    
    /**
     * D�finit la couleur du caract�re
     * @param c     nouvelle couleur
     */
    public void setColor(Color c)
    {
        this.colour = c;
    }
    
    /**
     * Renvoie la couleur courante du caract�re
     * @return  couleur courante
     */
    public Color getColor()
    {
        return this.colour;
    }
}
